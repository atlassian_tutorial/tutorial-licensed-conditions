# About this repository

This is the source code for [Tutorial: Omit web-resources when your plugin is unlicensed](https://developer.atlassian.com/x/u9sCAg). Use this repo to check your work, or inspect the code.

# Compatibility

The libraries used in this tutorial are compatible with:  

* JIRA 6.4+
* Confluence 5.6+

Or any Atlassian product using [atlassian-plugins-webresource](https://bitbucket.org/atlassian/atlassian-plugins-webresource) version 3.1+

# Tutorial overview

You'll learn how to exclude resources from the front-end when your plugin is unlicensed.

Your plugin will use the Licensing API to ensure compatibility with UPM. 

[Get started >>](https://developer.atlassian.com/x/u9sCAg)
