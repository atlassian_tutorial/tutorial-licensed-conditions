package com.example.plugins.conditions;

import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;

public class ConditionEvaluatorImpl implements ConditionEvaluator
{
    PluginLicenseManager licenseManager;

    public ConditionEvaluatorImpl(final PluginLicenseManager licenseManager)
    {
        this.licenseManager = licenseManager;
    }

    @Override
    public boolean evaluate(final ConditionType type)
    {
        switch (type) {
            case LICENSED:
                return isLicenseValid();
            default:
                return false;
        }
    }

    /**
     * Check the validity of our plugin's license.
     * @return true if the plugin's license is valid; false otherwise.
     */
    private boolean isLicenseValid()
    {
        // You should replace the logic here with more
        // appropriate logic, depending on how you license your plugin.
        boolean isLicensed = false;
        boolean hasErrors = false;
        for (PluginLicense pluginLicense : licenseManager.getLicense())
        {
            isLicensed = true;
            hasErrors = hasErrors || pluginLicense.getError().isDefined();
        }
        isLicensed = isLicensed && !hasErrors;
        return isLicensed;
    }
}
