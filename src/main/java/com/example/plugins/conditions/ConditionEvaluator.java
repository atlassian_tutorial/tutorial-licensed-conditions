package com.example.plugins.conditions;

public interface ConditionEvaluator
{
    public boolean evaluate(ConditionType type);
}
