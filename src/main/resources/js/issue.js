// You can read about how AMD works here: https://github.com/amdjs/amdjs-api/wiki/AMD
require([
    'jira/flag',
    'jquery'
], function(
    flag,
    $
) {
    var titleText = AJS.I18n.getText('my.plugin.viewissue.flag.title');
    var messageText = AJS.I18n.getText('my.plugin.viewissue.flag.message');

    $(function() {
        flag.showInfoMsg(titleText, messageText);
    });
});
