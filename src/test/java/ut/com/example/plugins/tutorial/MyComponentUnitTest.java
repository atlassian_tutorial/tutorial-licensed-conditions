package ut.com.example.plugins.tutorial;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.util.Option;
import com.example.plugins.tutorial.MyPluginComponent;
import com.example.plugins.tutorial.MyPluginComponentImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class MyComponentUnitTest
{
    @Mock PluginLicenseManager pluginLicenseManager;
    @Mock ApplicationProperties applicationProperties;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        when(pluginLicenseManager.getLicense()).thenReturn(getLicenses());
    }

    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(pluginLicenseManager, null);
        assertEquals("names do not match!", "myComponent", component.getName());
    }

    private Option<PluginLicense> getLicenses()
    {
        return Option.none();
    }
}
